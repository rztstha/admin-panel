<?php include('partials/header.php')  ?>




    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">


                    <div class="main-body">
                        <div class="page-wrapper">

                            <div class="row">

                                <div class="col-md-6 col-xl-3">
                                    <div class="card">
                                        <div class="card-block">
                                            <h6 class="mb-4">Total Banners</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-7">
                                                    <h3 class="f-w-300 d-flex align-items-center m-b-0"><i class="fa fa-photo text-c-green f-30 m-r-10"></i>5</h3>
                                                </div>
                                                <div class="col-5 text-right">
                                                   <a href="#"> <p class="m-b-0 small">View Info</p></a>
                                               </div>
                                           </div>

                                       </div>
                                   </div>
                               </div>

                               <div class="col-md-6 col-xl-3">
                                <div class="card">
                                    <div class="card-block">
                                        <h6 class="mb-4">Total Pages</h6>
                                        <div class="row d-flex align-items-center">
                                            <div class="col-7">
                                                <h3 class="f-w-300 d-flex align-items-center m-b-0"><i class="fa fa-file text-c-red f-30 m-r-10"></i>12</h3>
                                            </div>
                                            <div class="col-5 text-right">
                                               <a href="#"> <p class="m-b-0 small">View Info</p></a>
                                           </div>
                                       </div>

                                   </div>
                               </div>
                           </div>
                           <div class="col-md-6 col-xl-3">
                            <div class="card">
                                <div class="card-block">
                                    <h6 class="mb-4">Total Properties</h6>
                                    <div class="row d-flex align-items-center">
                                        <div class="col-7">
                                            <h3 class="f-w-300 d-flex align-items-center m-b-0"><i class="fa fa-building-o text-c-blue f-30 m-r-10"></i>5</h3>
                                        </div>
                                        <div class="col-5 text-right">
                                           <a href="#"> <p class="m-b-0 small">View Info</p></a>
                                       </div>
                                   </div>

                               </div>
                           </div>
                       </div>
                       <div class="col-md-6 col-xl-3">
                        <div class="card">
                            <div class="card-block">
                                <h6 class="mb-4">Total Enquries</h6>
                                <div class="row d-flex align-items-center">
                                    <div class="col-7">
                                        <h3 class="f-w-300 d-flex align-items-center m-b-0"><i class="fa fa-bell text-c-yellow f-30 m-r-10"></i>5</h3>
                                    </div>
                                    <div class="col-5 text-right">
                                       <a href="#"> <p class="m-b-0 small">View Info</p></a>
                                   </div>
                               </div>

                           </div>
                       </div>
                   </div>

                   <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Recent User Logs</h5>
                        </div>
                        <div class="card-block">

                            <div class="table-responsive">
                                <table id="scrolling-table" class="display table nowrap table-striped table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Name</th>
                                            <th>Log Info</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td>1</td>
                                            <td>Tiger Nixon</td>
                                            <td>Casa Deyra User admin logged in</td>
                                            <td>2011/04/25</td>
                                            <td>7:00 PM</td>
                                        </tr>
                                        <tr><td>2</td>
                                            <td>Jhon Nixon</td>
                                            <td>Casa Deyra User admin logged in</td>
                                            <td>2011/04/25</td>
                                            <td>7:00 PM</td>
                                        </tr>
                                        <tr><td>3</td>
                                            <td>Alexa Nixon</td>
                                            <td>Casa Deyra User admin logged in</td>
                                            <td>2011/04/25</td>
                                            <td>7:00 PM</td>
                                        </tr>
                                        

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
</div>
</div>
</div>



<?php include('partials/footer.php')  ?>