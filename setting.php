<?php include('partials/header.php')  ?>



<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Site Setting</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:">Site Setting</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <form method="post" action="" enctype="multipart/form-data" autocomplete="off" data-parsley-validate="" novalidate="">
                    <input type="hidden" name="_token" value="UHwovlVY6s4zvLR8ze3aN3WL0gX0Xo4AqhjfF7iy">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <div class="col-sm-8">
                                    <!-- [ form-element ] start -->
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Details</h5>
                                                <br> <small class="text-danger">* field are required to fill.</small>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                     <div class="form-group">
                                                        <label>Title <span class="text-danger">*</span> </label>
                                                        <input type="text" class="form-control" name="title" id="title" value="" placeholder="Enter Site Title" parsley-trigger="change" required="">
                                                    </div>
                                                    <span class="text-danger"></span>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>E-mail <span class="text-danger">*</span> </label>
                                                            <input type="email" class="form-control" name="email" id="email" value="" placeholder="Enter Email" parsley-trigger="change" required="">
                                                        </div>
                                                        <span class="text-danger"></span>
                                                        <div class="form-group col-md-6">
                                                            <label>Address <span class="text-danger">*</span> </label>
                                                            <input type="text" class="form-control" name="address" id="address" value="" placeholder="Enter Address" parsley-trigger="change" required="">
                                                        </div>
                                                        <span class="text-danger"></span>
                                                        <div class="form-group col-md-6">
                                                            <label>Tel No</label>
                                                            <input type="text" class="form-control" name="tel_no" id="tel_no" value="" placeholder="Enter Telephone Number">
                                                        </div>
                                                        <span class="text-danger"></span>
                                                        <div class="form-group col-md-6">
                                                            <label>Mobile No</label>
                                                            <input type="text" class="form-control" name="mobile_no" id="mobile_no" value="" placeholder="Enter Mobile No.">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Google Map Link</label>
                                                        <input type="text" class="form-control" name="google_map_link" id="google_map_link" value=" ">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Registration No</label>
                                                        <input type="text" class="form-control" name="reg_no" id="reg_no" value="" placeholder="Enter Registration No">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Contact Info for Advertisements</h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>E-mail</label>
                                                        <input type="email" class="form-control" name="adv_email" id="adv_email" value="" placeholder="Enter Email">
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>Tel No</label>
                                                            <input type="text" class="form-control" name="adv_tel_no" id="adv_tel_no" value="" placeholder="Enter Telephone Number">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Mobile No</label>
                                                            <input type="text" class="form-control" name="adv_mobile_no" id="adv_mobile_no" value="" placeholder="Enter Mobile No.">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Upload Logo Images</h5>
                                        </div>
                                        <div class="card-body">

                                            <div class="row">
                                                <div class="col-md-8 ">
                                                    <div class="form-group">
                                                        <label>Logo Image</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="image" name="footer_image" value="">
                                                            <label class="custom-file-label" for="image">Choose file...</label>
                                                            
                                                        </div>
                                                        <span class="text-danger"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="image-trap">
                                                        <div class="custom-control custom-checkbox select-1">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck" name="delete_image" value="delete_image" data-parsley-multiple="delete_image">
                                                            <label class="custom-control-label" for="customCheck" title="Check for delete this image"></label>
                                                        </div>
                                                        <a href="" data-toggle="lightbox" data-gallery="multiimages" data-title="" class=" img-responsive">
                                                            <img class="img-thumbnail image_list" src="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 "><div class="form-group">
                                                    <label>Favicon Image <span class="text-danger">*</span> </label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="iso_image" name="iso_image" value="">
                                                        <label class="custom-file-label" for="image">Choose file...</label>
                                                        
                                                    </div>
                                                    <span class="text-danger"></span>
                                                </div></div>
                                                <div class="col-md-4">
                                                    <div class="image-trap">
                                                        <div class="custom-control custom-checkbox select-1">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck" name="delete_image_2" value="delete_image_2" data-parsley-multiple="delete_image_2">
                                                            <label class="custom-control-label" for="customCheck" title="Check for delete this image"></label>
                                                        </div>
                                                        <a href="" data-toggle="lightbox" data-gallery="multiimages" data-title="" class=" img-responsive">
                                                            <img class="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                

                            </div>
                            <div class="col-sm-4">
                                <!-- [ form-element ] start -->
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Social Links</h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Facebook</label>
                                                        <input type="text" class="form-control" name="facebook" id="facebook" value="https://www.facebook.com" placeholder="Enter Facebook Link">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Instagram</label>
                                                        <input type="text" class="form-control" name="instagram" id="instagram" value="https://www.instagram.com" placeholder="Enter Instagram Link">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Twitter</label>
                                                        <input type="text" class="form-control" name="twitter" id="twitter" value="https://www.twitter.com" placeholder="Enter Twitter Link">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Linkedin</label>
                                                        <input type="text" class="form-control" name="linkedin" id="linkedin" value="https://www.linkedin.com" placeholder="Enter Linkedin Link">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Youtube</label>
                                                        <input type="text" class="form-control" name="youtube" id="youtube" value="https://www.youtube.com" placeholder="Enter Youtube Link">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Action</h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<?php include('partials/footer.php')  ?>


