<?php include('partials/header.php')  ?>



<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">

                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="index.php">Clients Testimonial</a></li>
                                    <li class="breadcrumb-item">Add Testimonial</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                 <div class="row">
                    <div class="col-sm-7">
                        <!-- [ form-element ] start -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Clients Information</h5>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="name" id="" value="" placeholder="Enter Clients Name" parsley-trigger="change" required="">
                                            </div>
                                            <span class="text-danger"></span>
                                            <div class="form-group">
                                                <label>Country</label>
                                                <input type="text" class="form-control" name="designation" id="" value="" placeholder="Enter Client's Country" required="">
                                            </div>
                                            <span class="text-danger"></span>
                                            <div class="form-group">
                                                <label for="content">Message</label>
                                                <textarea class="form-control" placeholder="Enter Client's Message..."  rows="5"></textarea>

                                            </div>
                                            <div class="row">
                                                <div class=" col-md-12 "><div class="form-group">
                                                    <label>Upload Client's Image</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="image" name="image" value="">
                                                        <label class="custom-file-label" for="image">Choose file...</label>
                                                        <div class="invalid-feedback">Example invalid custom file feedback</div>
                                                    </div>
                                                    <span class="text-danger"></span>
                                                </div></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Review From</label>
                                                <input type="text" class="form-control"  name="phone" value="" aria-describedby="emailHelp" placeholder="Enter Review From">
                                            </div>

                                            <div class="form-group">
                                                <label>Review URL</label>
                                                <input type="text" class="form-control" name="email_id" id="email_id" value="" placeholder="Enter Review Url">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Properties Details</h5>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label for="status">Properties</label>

                                                <select class="form-control js-example-basic-hide-search col-sm-12" multiple="multiple" style="width: 75%">
                                                    <optgroup label="Choose Property">
                                                        <option >Golden Hour Breezy 1BR Apartment @Thamel</option>
                                                        <option >Homestyle 1BR Apartment @Thamel</option>
                                                    </optgroup>
                                                </select>
                                            </div>

                                              <div class="form-group">
                                                <label for="status">Checked In As</label>
                                                <select class="form-control" id="status" name="status" required="">
                                                    <option value="">Guest</option>
                                                    <option value="">Host</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Action</h5>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <select class="form-control" id="status" name="status" required="">
                                                    <option value="active">Active</option>
                                                    <option value="in_active">Inactive</option>
                                                </select>
                                            </div>
                                            <span class="text-danger"></span>
                                            <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
</div>
</div>
</div>
</div>



<?php include('partials/footer.php')  ?>