<?php include('partials/header.php')  ?>



<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
               <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Clients Testimonials</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:">Clients Testimonials</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


             <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Clients Testimonial Management</h5>
                        <a href="add.php"><button type="button" class="btn btn-outline-primary float-right" ><i class="fa fa-plus"></i> Add Testimonial</button></a>
                    </div>
                    <div class="card-block">

                      <div class="table-responsive">
                        <table id="responsive-table-model" class="display table dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Full Name</th>
                                    <th>Country</th>
                                    <th>Guest / Host</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Nixon Swift</td>
                                    <td>Newzealand</td>
                                    <td>Guest</td>
                                    <td><a href="#!" class="label text-c-green f-16"><i class="fa fa-check"></a></td>
                                    <td><a href="#!" class="label text-c-red f-16"><i class="fa fa-trash"></i></a><a href="#!" class="label text-c-blue f-16"><i class="fa fa-edit"></a></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
</div>
</div>
</div>
</div>



<?php include('partials/footer.php')  ?>