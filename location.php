<?php include('partials/header.php')  ?>



<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                 <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Loction List</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="">Property Management</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:">Location List</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


             <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Location List</h5>
                        <a href="add.php"><button type="button" class="btn btn-outline-primary float-right" ><i class="fa fa-plus"></i> Add Location</button></a>
                    </div>
                    <div class="card-block">

                      <div class="table-responsive">
                        <table id="responsive-table-model" class="display table dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Location Title</th>
                                    <th>Post Code</th>
                                    <th>Location</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Patan</td>
                                    <td>44800</td>
                                    <td>Kathmandu</td>
                                    <td><a href="#!" class="label text-c-red f-16"><i class="fa fa-times"></a></td>
                                    <td><a href="#!" class="label text-c-red f-16"><i class="fa fa-trash"></i></a><a href="#!" class="label text-c-blue f-16"><i class="fa fa-edit"></a></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
</div>
</div>
</div>
</div>



<?php include('partials/footer.php')  ?>