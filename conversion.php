<?php include('partials/header.php')  ?>



<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                 <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Conversion</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="http://test.kheladi.com/super-admin/dashboard">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:">Conversion</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


             <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Conversion Management</h5>
                        <button type="button" class="btn btn-outline-primary float-right" ><i class="fa fa-plus"></i> Add Conversion</button>
                    </div>
                    <div class="card-block">

                      <div class="table-responsive">
                        <table id="responsive-table-model" class="display table dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Property Type</th>
                                    <th>Location</th>
                                    <th>Price</th>
                                    <th>Customer Name</th>
                                    <th>Date</th>
                                    <th>Phone</th>
                                    <th>E-mail</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Lazimpat</td>
                                    <td>Studio Apartment</td>
                                    <td>1500</td>
                                    <td>Nixon Swift</td>
                                    <td>2011/04/25</td>
                                    <td>987654321</td>    
                                    <td>nixsonswt@gmail.com</td>
                                    <td><a href="#!" class="label text-c-red f-16"><i class="fa fa-trash"></i></a><a href="#!" class="label text-c-green f-16"><i class="fa fa-check"></a></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
</div>
</div>
</div>
</div>



<?php include('partials/footer.php')  ?>