<!--[if lt IE 11]>
        <div class="ie-warning">
            <h1>Warning!!</h1>
            <p>You are using an outdated version of Internet Explorer, please upgrade
               <br/>to any of the following web browsers to access this website.
            </p>
            <div class="iew-container">
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="assets/images/browser/chrome.png" alt="Chrome">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="assets/images/browser/firefox.png" alt="Firefox">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="assets/images/browser/opera.png" alt="Opera">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="assets/images/browser/safari.png" alt="Safari">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="assets/images/browser/ie.png" alt="">
                            <div>IE (11 & above)</div>
                        </a>
                    </li>
                </ul>
            </div>
            <p>Sorry for the inconvenience!</p>
        </div>
    <![endif]-->


    <script src="assets/js/vendor-all.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/menu-setting.min.js"></script>
    <script src="assets/js/pcoded.min.js"></script>

    <script src="assets/plugins/amchart/js/amcharts.js"></script>
    <script src="assets/plugins/amchart/js/gauge.js"></script>
    <script src="assets/plugins/amchart/js/serial.js"></script>
    <script src="assets/plugins/amchart/js/light.js"></script>
    <script src="assets/plugins/amchart/js/pie.min.js"></script>
    <script src="assets/plugins/amchart/js/ammap.min.js"></script>
    <script src="assets/plugins/amchart/js/usaLow.js"></script>
    <script src="assets/plugins/amchart/js/radar.js"></script>
    <script src="assets/plugins/amchart/js/worldLow.js"></script>

    <script src="assets/plugins/notification/js/bootstrap-growl.min.js"></script>
    <script src="assets/plugins/data-tables/js/datatables.min.js"></script>
    <script src="assets/js/pages/tbl-datatable-custom.js"></script>
    <script src="assets/js/pages/dashboard-custom.js"></script>
    <script src="assets/plugins/select2/js/select2.full.min.js"></script>

<script src="assets/plugins/multi-select/js/jquery.quicksearch.js"></script>
<script src="assets/plugins/multi-select/js/jquery.multi-select.js"></script>

<script src="assets/js/pages/form-select-custom.js"></script>
</body>

<!-- Mirrored from html.codedthemes.com/datta-able/bootstrap/default/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Jul 2020 05:36:15 GMT -->
</html>
